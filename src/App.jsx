import './App.css';
import Experience from '../src/components/Experience/Experience.jsx';
import Feedback from '../src/components/Feedback/Feedback.jsx';
import Panel from '../src/components/Panel/Panel.jsx';
import Portfolio from '../src/components/Portfolio/Portfolio.jsx';
import About from '../src/components/About/About.jsx';
import Contacts from '../src/components/Contacts/Contacts.jsx';
import Timeline from '../src/components/TimeLine/TimeLine.jsx';
import Entry from '../src/components/Entry/Entry.jsx';
import Skills from '../src/components/Skills/Skills.jsx';
import { Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import { makeServer } from '../src/services/server.js';
if (process.env.NODE_ENV === 'development') {
	makeServer();
}
function App() {
	const [timelineData, setTimelineData] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [skillsData, setSkillsData] = useState([]);

	useEffect(() => {
		const fetchData = async () => {
			try {
				const [timelineResponse, skillsResponse] = await Promise.all([
					fetch('/api/timeline'),
					fetch('/api/skills'),
				]);

				const timelineJson = await timelineResponse.json();
				const skillsJson = await skillsResponse.json();

				setTimelineData(timelineJson.timelineItems);
				setSkillsData(skillsJson.skillsItems);
			} catch (error) {
				console.error('Error fetching data:', error);
			} finally {
				setIsLoading(false);
			}
		};

		fetchData();
	}, []);

	return (
		<div className='App'>
			<Routes>
				<Route path='/' element={<Entry />} />
				<Route
					path='/cv'
					element={
						<>
							<Panel />
							<div className='main-container'>
								<About />
								<Timeline data={timelineData} />
								{isLoading && (
									<div id='loading'>
										<svg
											width='30px'
											height='30px'
											fill='#26C17E'
											xmlns='http://www.w3.org/2000/svg'
											viewBox='0 0 512 512'
										>
											<path d='M370.7 133.3C339.5 104 298.9 88 255.8 88c-77.5 .1-144.3 53.2-162.8 126.9-1.3 5.4-6.1 9.2-11.7 9.2H24.1c-7.5 0-13.2-6.8-11.8-14.2C33.9 94.9 134.8 8 256 8c66.4 0 126.8 26.1 171.3 68.7L463 41C478.1 25.9 504 36.6 504 57.9V192c0 13.3-10.7 24-24 24H345.9c-21.4 0-32.1-25.9-17-41l41.8-41.7zM32 296h134.1c21.4 0 32.1 25.9 17 41l-41.8 41.8c31.3 29.3 71.8 45.3 114.9 45.3 77.4-.1 144.3-53.1 162.8-126.8 1.3-5.4 6.1-9.2 11.7-9.2h57.3c7.5 0 13.2 6.8 11.8 14.2C478.1 417.1 377.2 504 256 504c-66.4 0-126.8-26.1-171.3-68.7L49 471C33.9 486.1 8 475.4 8 454.1V320c0-13.3 10.7-24 24-24z' />
										</svg>
									</div>
								)}
								<Skills data={skillsData} />
								<Experience
									data={[
										{
											date: '2013-2014',
											info: {
												company: 'Google',
												job: 'Front-end developer / php programmer',
												description:
													'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
											},
										},
										{
											date: '2012',
											info: {
												company: 'Twitter',
												job: 'Web developer',
												description:
													'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
											},
										},
									]}
								/>
								<Portfolio />
								<Contacts />
								<Feedback
									data={[
										{
											feedback:
												' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ',
											reporter: {
												photoUrl: '/assets/mark.jpeg',
												name: 'Mark Zuckerberg',
												citeUrl: 'https://www.citeexample.com',
											},
										},
										{
											feedback:
												' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ',
											reporter: {
												photoUrl: '/assets/steve.jpeg',
												name: 'Steve Jobs',
												citeUrl: 'https://www.citeexample.com',
											},
										},
									]}
								/>
								<div className='arrow-up'>
									<a href='#about-me'>&#x2303;</a>
								</div>
							</div>
						</>
					}
				/>
			</Routes>
		</div>
	);
}

export default App;
