import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/app/store.js'; // Adjust the path to your Redux store
import App from '../src/App.jsx'; // Adjust the path to your App component

test('renders App component', () => {
	render(
		<Provider store={store}>
			<BrowserRouter>
				<App />
			</BrowserRouter>
		</Provider>
	);

	// Add your test assertions here
});
