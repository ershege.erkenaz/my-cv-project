import { createServer, Model } from 'miragejs';

export function makeServer({ environment = 'development' } = {}) {
	const loadFromLocalStorage = () => {
		try {
			const serializedState = localStorage.getItem('skills');
			if (serializedState === null) {
				return [];
			}
			return JSON.parse(serializedState);
		} catch (e) {
			console.warn('Could not load skills from local storage', e);
			return [];
		}
	};
	let server = createServer({
		environment,

		models: {
			timelineItem: Model,
			skillsItem: Model,
		},

		seeds(server) {
			const skills = loadFromLocalStorage() || [
				{ name: 'JavaScript', range: 50 },
				{ name: 'React', range: 70 },
				{ name: 'Node.js', range: 40 },
			];
			const timelineData = [
				{
					date: 2001,
					title: 'Title 0',
					text: 'Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.',
				},
				{
					date: 2000,
					title: 'Title 1',
					text: 'Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.',
				},
				{
					date: 2012,
					title: 'Title 2',
					text: 'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.',
				},
				{
					date: 2001,
					title: 'Title 0',
					text: 'Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.',
				},
				{
					date: 2000,
					title: 'Title 1',
					text: 'Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.',
				},
				{
					date: 2012,
					title: 'Title 2',
					text: 'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.',
				},
			];

			timelineData.forEach((item) => {
				server.create('timelineItem', item);
			});

			skills.forEach((item) => {
				server.create('skillsItem', item);
			});
		},

		routes() {
			this.namespace = 'api';

			this.get(
				'/timeline',
				(schema) => {
					return schema.timelineItems.all();
				},
				{ timing: 3000 }
			);

			this.get('/skills', (schema) => {
				return schema.skillsItems.all();
			});

			this.post('/skills', (schema, request) => {
				let attrs = JSON.parse(request.requestBody);
				return schema.skillsItems.create(attrs);
			});
		},
	});

	return server;
}
