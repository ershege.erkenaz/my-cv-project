import { configureStore } from '@reduxjs/toolkit';
import educationReducer from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/features/education/educationSlice.js';
import skillsReducer from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/features/skills/skillSlice.js';

const store = configureStore({
	reducer: {
		education: educationReducer,
		skills: skillsReducer,
	},
	devTools: true,
});

export default store;
