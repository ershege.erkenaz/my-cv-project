import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	list: [],
};

const educationSlice = createSlice({
	name: 'education',
	initialState,
	reducers: {
		addEducation: (state, action) => {
			state.list.push(action.payload);
		},
		removeEducation: (state, action) => {
			state.list = state.list.filter((item) => item.id !== action.payload.id);
		},
	},
});

export const { addEducation, removeEducation } = educationSlice.actions;
export default educationSlice.reducer;
