import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const loadFromLocalStorage = () => {
	try {
		const serializedState = localStorage.getItem('skills');
		if (serializedState === null) {
			return [];
		}
		return JSON.parse(serializedState);
	} catch (e) {
		console.warn('Could not load skills from local storage', e);
		return [];
	}
};

const saveToLocalStorage = (skills) => {
	try {
		const serializedState = JSON.stringify(skills);
		localStorage.setItem('skills', serializedState);
	} catch (e) {
		console.warn('Could not save skills to local storage', e);
	}
};

const initialState = {
	items: loadFromLocalStorage() || [],
	status: 'idle',
	error: null,
};

export const fetchSkills = createAsyncThunk('skills/fetchSkills', async () => {
	const response = await fetch('/api/skills');
	const data = await response.json();
	return data.skillsItems;
});

const skillsSlice = createSlice({
	name: 'skills',
	initialState,
	reducers: {
		addSkill: (state, action) => {
			state.items.push(action.payload);
			saveToLocalStorage(state.items);
		},
	},
	extraReducers: (builder) => {
		builder
			.addCase(fetchSkills.pending, (state) => {
				state.status = 'loading';
			})
			.addCase(fetchSkills.fulfilled, (state, action) => {
				state.status = 'succeeded';
				state.items = action.payload;
			})
			.addCase(fetchSkills.rejected, (state, action) => {
				state.status = 'failed';
				state.error = action.error.message;
			});
	},
});

export const { addSkill } = skillsSlice.actions;
export default skillsSlice.reducer;
