import './Experience.css';
import Box from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Box/Box.jsx';
function Experience({ data }) {
	const content = data.map((experience, index) => (
		<table key={index}>
			<tbody>
				<tr>
					<th className='table-right-half'>{experience.info.company}</th>
					<th>{experience.info.job}</th>
				</tr>
				<tr>
					<td className='table-right-half'>{experience.date}</td>
					<td>{experience.info.description}</td>
				</tr>
			</tbody>
		</table>
	));
	return <Box id='experience' title='Experience' content={content} />;
}

export default Experience;
