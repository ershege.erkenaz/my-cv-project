import './Feedback.css';
import Info from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Info/Info.jsx';
import Box from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Box/Box.jsx';

function Feedback({ data }) {
	const feedbackContent = data.map((feedback, index) => (
		<div key={index}>
			<Info text={feedback.feedback} />
			<div className='author-block'>
				<img
					className='feedback-img'
					src={feedback.reporter.photoUrl}
					alt={feedback.reporter.name}
				/>
				<p className='feedback-author'>
					{feedback.reporter.name}
					{', '}
					<a href={feedback.reporter.citeUrl}>somesite.com</a>
				</p>
			</div>
		</div>
	));

	return <Box id='feedback' title='Feedbacks' content={feedbackContent} />;
}

export default Feedback;
