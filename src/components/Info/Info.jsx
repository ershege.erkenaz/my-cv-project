const styles = {
	backgroundColor: '#EEEEEE',
	padding: 10,
	marginTop: 50,
};
function Info(props) {
	return <p style={styles}>{props.text}</p>;
}
export default Info;
