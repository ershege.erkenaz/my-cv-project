import React from 'react';
import PhotoBox from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/PhotoBox/PhotoBox.jsx';
import Button from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Button/Button.jsx';
import { Link } from 'react-router-dom';
import './Entry.css';

const Entry = ({ data }) => {
	return (
		<div className='container'>
			<PhotoBox />
			<h4>Programmer. Creative. Innovator</h4>
			<p>
				I like coffee. I like coffee. I like coffee. I like coffee. I like
				coffee. I like coffee. I like coffee. I like coffee.
			</p>
			<Link to='/cv'>
				<Button text='Know More' />
			</Link>
		</div>
	);
};

export default Entry;
