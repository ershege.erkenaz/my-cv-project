import React, { useState } from 'react';

const SkillForm = ({ onAddSkill }) => {
	const [name, setName] = useState('');
	const [range, setRange] = useState('');
	const [errors, setErrors] = useState({});
	const styles = {
		border: '1px solid green',
		padding: '25px',
		borderRadius: '5px',
		fontSize: 16,
		fontFamily: 'Open Sans',
		marginBottom: 30,
	};
	const handleSubmit = (e) => {
		e.preventDefault();
		if (validateForm()) {
			onAddSkill({ name, range: parseInt(range, 10) });
			setName('');
			setRange('');
		}
	};
	const validateForm = () => {
		let errors = {};
		let isValid = true;

		if (!name.trim()) {
			errors.name = 'Skill name is a required field';
			isValid = false;
		}

		if (!range.trim()) {
			errors.range = 'Skill range is a required field';
			isValid = false;
		} else if (
			isNaN(range) ||
			parseInt(range, 10) < 10 ||
			parseInt(range, 10) > 100
		) {
			errors.range = 'Skill range must be a number from 10 to 100';
			isValid = false;
		}

		setErrors(errors);
		return isValid;
	};
	return (
		<form style={styles} onSubmit={handleSubmit}>
			<div style={{ margin: '15px 0' }}>
				<label>
					<b>Skill Name:</b>
					<input
						style={{ marginLeft: '10px' }}
						placeholder='  Enter skill name'
						type='text'
						value={name}
						onChange={(e) => setName(e.target.value)}
					/>
				</label>
				{errors.name && (
					<p style={{ color: 'red', fontSize: '14px' }}>{errors.name}</p>
				)}
			</div>
			<div style={{ margin: '15px 0' }}>
				<label>
					<b>Skill Range:</b>
					<input
						style={{ marginLeft: '10px' }}
						placeholder='  Enter skill range'
						type='number'
						value={range}
						onChange={(e) => setRange(e.target.value)}
					/>
				</label>
				{errors.range && (
					<p style={{ color: 'red', fontSize: '14px' }}>{errors.range}</p>
				)}
			</div>
			<button style={{ position: 'initial' }} type='submit'>
				Add Skill
			</button>
		</form>
	);
};

export default SkillForm;
