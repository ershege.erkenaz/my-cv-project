import React, { useState, useEffect } from 'react';
import Box from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Box/Box.jsx';
import SkillForm from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Skills/SkillForm.jsx';
import Button from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Button/Button.jsx';
import {
	fetchSkills,
	addSkill,
} from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/features/skills/skillSlice.js';
import './Skills.css';
import { useSelector, useDispatch } from 'react-redux';
const Skills = ({ data }) => {
	const skillsData = useSelector((state) => state.skills.items);
	// const state = useSelector((state) => state);
	const skillsStatus = useSelector((state) => state.skills.status);
	const dispatch = useDispatch();
	const [isFormVisible, setIsFormVisible] = useState(false);

	useEffect(() => {
		if (skillsStatus === 'idle') {
			dispatch(fetchSkills());
		}
	}, [skillsStatus, dispatch]);

	const handleAddSkill = async (newSkill) => {
		dispatch(addSkill(newSkill));
	};

	const handleEdit = () => {
		setIsFormVisible(true);
	};

	const handleCancel = () => {
		setIsFormVisible(false);
	};

	const content = Array.isArray(skillsData) ? (
		<div className='skills-container'>
			<Button text='Edit' onClick={handleEdit} />
			{isFormVisible && (
				<SkillForm onAddSkill={handleAddSkill} onCancel={handleCancel} />
			)}
			{skillsData.map((item, index) => (
				<div className='skills-item' data-testid='skill-name' key={index}>
					<div className='skills-name'>
						<span>{item.name}</span>
					</div>
					<div className='skills-range'>
						<div className='bar' style={{ width: `${item.range}%` }}></div>
					</div>
				</div>
			))}
			<div className='bar-container'>
				<div className='bar' style={{ width: '0%' }}></div>
				<div className='bar' style={{ width: '33%' }}></div>
				<div className='bar' style={{ width: '66%' }}></div>
				<div className='bar' style={{ width: '100%' }}></div>
			</div>
			<div className='bar-container'>
				<div className='label'>Beginner</div>
				<div className='label'>Proficient</div>
				<div className='label'>Expert</div>
				<div className='label'>Master</div>
			</div>
		</div>
	) : null;
	return (
		<>
			<Box id='skills' title='Skills' content={content} />
		</>
	);
};

export default Skills;
