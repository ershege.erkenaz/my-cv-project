// src/components/Skills/Skills.test.js

import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Skills from './Skills';
import { Provider } from 'react-redux'; // Assuming you're using Redux
import configureStore from 'redux-mock-store'; // Import mock store

// Mock Redux store
const mockStore = configureStore([]);

describe('Skills Component', () => {
	let store;

	beforeEach(() => {
		store = mockStore({
			skills: {
				items: [
					{ id: 1, name: 'JavaScript', range: 80 },
					{ id: 2, name: 'React', range: 90 },
				],
				status: 'idle', // Mock the initial status
			},
		});

		// Mock the fetchSkills action
		store.dispatch = jest.fn();
	});

	test('renders Skills component', () => {
		render(
			<Provider store={store}>
				<Skills />
			</Provider>
		);

		// Check if the "Edit" button is rendered
		expect(screen.getByText('Edit')).toBeInTheDocument();

		// Check if skills items are rendered
		const skillNames = screen.getAllByTestId('skill-name');
		expect(skillNames).toHaveLength(2); // Adjust the count based on your mocked data
	});

	test('toggles SkillForm visibility on "Edit" click', () => {
		render(
			<Provider store={store}>
				<Skills />
			</Provider>
		);
	});
});
