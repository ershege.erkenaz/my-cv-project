import Navigation from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Navigation/Navigation.jsx';
import Button from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Button/Button.jsx';
import PhotoBox from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/PhotoBox/PhotoBox.jsx';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './Panel.css';
function Panel(props) {
	const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth <= 600);
	useEffect(() => {
		const handleResize = () => {
			setIsSmallScreen(window.innerWidth <= 600);
		};
		window.addEventListener('resize', handleResize);
		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, []);
	function hamburgerOpen(event) {
		const block = document.querySelector('.dark-box');
		const hamburger = document.querySelector('.hamburger');
		const smallBlock = document.querySelector('.dark-box-without-ham');
		const main = document.querySelector('.main-container');
		const isSmallScreen = window.innerWidth <= 600;
		if (block.classList.contains('open')) {
			block.classList.remove('open');
			hamburger.style.left = '-5px'; // Move hamburger outside the hidden panel
			smallBlock.style.width = '0px';
			smallBlock.style.paddingLeft = '0px';
			main.style.marginLeft = '40px';
		} else {
			block.classList.add('open');
			if (isSmallScreen) {
				hamburger.style.left = '45px'; // Adjust position for small screens
				smallBlock.style.width = '40px';
				smallBlock.style.paddingLeft = '15px';
				main.style.marginLeft = '60px';
			} else {
				main.style.marginLeft = '250px';
				hamburger.style.left = '236px';
				smallBlock.style.width = '200px';
				smallBlock.style.paddingLeft = '40px'; // Adjust position for larger screens
			}
		}
	}
	return (
		<div className='dark-box open'>
			<div className='hamburger' onClick={hamburgerOpen}>
				&#x2630;
			</div>
			<div className='dark-box-without-ham'>
				<PhotoBox />
				<Navigation />
				<Link to='/'>
					<Button text={isSmallScreen ? '<' : '< GO BACK'} />
				</Link>
			</div>
		</div>
	);
}
export default Panel;
