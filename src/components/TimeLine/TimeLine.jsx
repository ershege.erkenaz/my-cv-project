import React from 'react';
import Box from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Box/Box.jsx';
import './TimeLine.css';

const Timeline = ({ data }) => {
	const content = Array.isArray(data) ? (
		<div className='timeline-container'>
			{data.map((item, index) => (
				<div className='timeline-item' key={index}>
					<div className='timeline-date'>
						<span>{item.date}</span>
					</div>
					<div className='timeline-content'>
						<h3>{item.title}</h3>
						<p>{item.text}</p>
					</div>
				</div>
			))}
		</div>
	) : null;
	return <Box id='education' title='Education' content={content} />;
};

export default Timeline;
