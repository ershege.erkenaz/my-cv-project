// src/components/Timeline/Timeline.test.js

import React from 'react';
import { render, screen } from '@testing-library/react';
import Timeline from './Timeline';

describe('Timeline Component', () => {
	const testData = [
		{ date: '2022', title: 'Title 1', text: 'Text 1' },
		{ date: '2023', title: 'Title 2', text: 'Text 2' },
	];

	test('renders Timeline component with data', () => {
		render(<Timeline data={testData} />);

		// Check if the component renders the title correctly
		expect(screen.getByText('Education')).toBeInTheDocument();

		// Check if each timeline item is rendered with correct content
		testData.forEach((item) => {
			expect(screen.getByText(item.date)).toBeInTheDocument();
			expect(screen.getByText(item.title)).toBeInTheDocument();
			expect(screen.getByText(item.text)).toBeInTheDocument();
		});
	});

	test('renders null with empty data array', () => {
		render(<Timeline data={[]} />);

		// Check if the component renders null when data array is empty
		expect(screen.queryByTestId('timeline-item')).not.toBeInTheDocument();
	});

	test('renders null with null data', () => {
		render(<Timeline data={null} />);

		// Check if the component renders null when data is null
		expect(screen.queryByTestId('timeline-item')).not.toBeInTheDocument();
	});

	test('renders null with undefined data', () => {
		render(<Timeline />);

		// Check if the component renders null when data is undefined
		expect(screen.queryByTestId('timeline-item')).not.toBeInTheDocument();
	});
});
