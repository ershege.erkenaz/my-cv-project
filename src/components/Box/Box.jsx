const styles = {
	margin: 'auto',
	marginBottom: '10px',
	padding: '15px 30px',
	// backgroundColor: 'bisque',
};
function Box(props) {
	return (
		<div id={props.id} style={styles}>
			<h2 style={{ color: '#26C17E' }}>{props.title}</h2>
			{props.content}
		</div>
	);
}
export default Box;
