import './PhotoBox.css';
function PhotoBox(props) {
	return (
		<div>
			<img className='user-photo' src='/assets/progile.jpeg' alt='' />
			<h3 className='name'>Yerkenaz Yershege</h3>
		</div>
	);
}
export default PhotoBox;
