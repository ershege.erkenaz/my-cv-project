import React, { useEffect } from 'react';
import Isotope from 'isotope-layout';
import './Portfolio.css';
import Box from '/Users/yerkenaz/Desktop/programming/JavaScript/CV-app/src/components/Box/Box.jsx';
const Portfolio = () => {
	useEffect(() => {
		const grid = document.querySelector('.grid');
		const iso = new Isotope(grid, {
			itemSelector: '.grid-item',
			layoutMode: 'masonry',
			masonry: {
				columnWidth: '.grid-sizer',
			},
		});

		const filters = document.querySelectorAll('.filter-button-group button');
		filters.forEach((button) => {
			button.addEventListener('click', () => {
				const filterValue = button.getAttribute('data-filter');
				iso.arrange({ filter: filterValue });
			});
		});

		return () => {
			iso.destroy();
			filters.forEach((button) => {
				button.removeEventListener('click', () => {
					const filterValue = button.getAttribute('data-filter');
					iso.arrange({ filter: filterValue });
				});
			});
		};
	}, []);
	const content = (
		<div>
			<div className='filter-button-group'>
				<button data-filter='*'>Show All</button>
				<button data-filter='.entertainment'>Entertainment</button>
				<button data-filter='.nature'>Nature</button>
				<button data-filter='.education'>Education</button>
			</div>
			<div className='grid'>
				<div className='grid-sizer'></div>
				<div data-testid='grid-item' className='grid-item entertainment'>
					<img src='/assets/art.jpeg' alt='' />
					<div className='description'>
						<h4>Gallery</h4>
						<p>
							Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
							arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
							justo. Nullam dictum felis eu pede mollis
						</p>
					</div>
				</div>
				<div className='grid-item nature'>
					<img src='/assets/tours.jpeg' alt='' />
					<div className='description'>
						<h4>Tours</h4>
						<p>
							Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
							arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
							justo. Nullam dictum felis eu pede mollis
						</p>
					</div>
				</div>
				<div className='grid-item entertainment'>
					<img src='/assets/cinema.jpeg' alt='' />
					<div className='description'>
						<h4>Online Cinema</h4>
						<p>
							Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
							arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
							justo. Nullam dictum felis eu pede mollis
						</p>
					</div>
				</div>
				<div className='grid-item education'>
					<img src='/assets/courses.jpeg' alt='' />
					<div className='description'>
						<h4>Design Courses</h4>
						<p>
							Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
							arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
							justo. Nullam dictum felis eu pede mollis
						</p>
					</div>
				</div>
				<div className='grid-item education'>
					<img src='/assets/cookbook.png' alt='' />
					<div className='description'>
						<h4>CookBook</h4>
						<p>
							Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
							arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
							justo. Nullam dictum felis eu pede mollis
						</p>
					</div>
				</div>
				<div className='grid-item nature'>
					<img src='/assets/camp.gif' alt='' />
					<div className='description'>
						<h4>Camping</h4>
						<p>
							Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
							arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
							justo. Nullam dictum felis eu pede mollis
						</p>
					</div>
				</div>
			</div>
		</div>
	);

	return <Box id='portfolio' title='Portfolio' content={content} />;
};

export default Portfolio;
