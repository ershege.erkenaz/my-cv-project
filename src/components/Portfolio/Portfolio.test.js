import React from 'react';
import { render, screen } from '@testing-library/react';
import Portfolio from './Portfolio';

describe('Portfolio component', () => {
	it('renders Portfolio component', () => {
		render(<Portfolio />);

		// Check if the title "Portfolio" is rendered
		const titleElement = screen.getByText(/Portfolio/i);
		expect(titleElement).toBeInTheDocument();

		// Check if filter buttons are rendered
		const showAllButton = screen.getByText(/Show All/i);
		expect(showAllButton).toBeInTheDocument();
		const entertainmentButton = screen.getByText(/Entertainment/i);
		expect(entertainmentButton).toBeInTheDocument();
		const natureButton = screen.getByText(/Nature/i);
		expect(natureButton).toBeInTheDocument();
		const educationButton = screen.getByText(/Education/i);
		expect(educationButton).toBeInTheDocument();

		// Check if grid items are rendered
		const gridItems = screen.getAllByTestId('grid-item');
		expect(gridItems.length).toBe(1); // Adjust the number based on your actual items

		// Check if each grid item contains expected content
		const galleryItem = screen.getByText(/Gallery/i);
		expect(galleryItem).toBeInTheDocument();
		const toursItem = screen.getByText(/Tours/i);
		expect(toursItem).toBeInTheDocument();
		const onlineCinemaItem = screen.getByText(/Online Cinema/i);
		expect(onlineCinemaItem).toBeInTheDocument();
		const designCoursesItem = screen.getByText(/Design Courses/i);
		expect(designCoursesItem).toBeInTheDocument();
		const cookBookItem = screen.getByText(/CookBook/i);
		expect(cookBookItem).toBeInTheDocument();
		const campingItem = screen.getByText(/Camping/i);
		expect(campingItem).toBeInTheDocument();
	});

	// Add more specific tests as needed
});
