import './Button.css';

function Button(props) {
	return (
		<button icon={props.icon} onClick={props.onClick}>
			{props.text}
		</button>
	);
}

export default Button;
